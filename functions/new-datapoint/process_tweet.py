from sentiment import get_tweet_sentiment
from bayes_classifier import create_classifier, word_feats
from find_helper import find_urls, find_hashtags, find_mentions

def process_tweet(tweet):
	
	small_text = ''.join([i.lower() if i.isalpha() else i for i in tweet]).encode('utf-8').strip()


	#Sentiment stuff
	_sentiment = get_tweet_sentiment(tweet)

	sentiment = 0.5
	if _sentiment == 'positive':
		sentiment = 1
	elif _sentiment == 'negative':
		sentiment = 0


	# Bayes stuff
	classifier = create_classifier()

	feats = word_feats(small_text)


	classification = classifier.prob_classify(feats).prob('android')

	urls = find_urls(tweet)

	# replace urls
	number_urls = len(urls)
	for url in urls:
		tweet = tweet.replace(url,'')

	# replace hashtags
	hashtags = find_hashtags(tweet)
	number_hashtags = len(hashtags)
	for hashtag in hashtags:
		tweet = tweet.replace('#{}'.format(hashtag),'')

	# replace mentions
	mentions = find_mentions(tweet)
	number_mentions = len(mentions)
	for mention in mentions:
		tweet = tweet.replace('@{}'.format(mention),'')

	tweet = tweet.replace('  ',' ')

	percentage_caps = 0 if not len(tweet) else sum(1 for c in tweet if c.isupper()) * 1.0 / len(tweet)

	result = [
		number_urls,
		number_hashtags,
		number_mentions,
		sentiment,
		percentage_caps,
		classification
	]

	return result