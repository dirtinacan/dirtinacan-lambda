import pickle, os

def word_feats(sentence):
	words = sentence.replace('#',"# ").replace('@','@ ').split(' ')
	return dict([('contains({})'.format(word), True) for word in words])

def create_classifier():
	f = open(os.path.join(os.path.dirname(__file__), 'classifier.pickle'), 'rb')
	classifier = pickle.load(f)
	f.close()
	return classifier

if __name__ == '__main__':
	create_classifier()
