import re

def find_urls(text):
	return re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)

def find_hashtags(text):
	return re.findall(r"#(\w+)", text)

def find_mentions(text):
	return re.findall(r"@(\w+)", text)