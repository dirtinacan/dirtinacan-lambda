# -*- coding: utf-8 -*-

# Add pip modules folder to path so we can see installed modules
import sys, os, datetime, json
sys.path.append(os.path.join(os.path.dirname(__file__), "pip_modules"))
import boto3, zipfile

from cloudant.client import Cloudant
from cloudant.query import Query

import facebook

from twitter import *
twitter = Twitter(
  auth=OAuth(
    "718819812391153664-oaWbt29mldsgK9R4lSaB6Pl4MqGK07s",
    "YLdXkWuRZ7HL2xULJXeJGjJ9eKtRMKk1uupqLEMYeuGge",
    "Lviw4dFJUwCxRLtuFT1hMwurR",
    "uat5XBjqSznRej7zCh5AkhlhUn38osW7DRG1KA1nPbWUWvH8qY"
  )
)

print("Starting Printing")
# Add pip modules folder to path so we can see installed modules
s3 = boto3.resource('s3')
downloaded_zip = '/tmp/tensorflow.zip'
print("Downloading tensorflow")
s3.meta.client.download_file('python-modules-diac', 'tensorflow.zip', downloaded_zip)
print("Finished downloading tensorflow")
zip_ref = zipfile.ZipFile(downloaded_zip, 'r')
zip_ref.extractall('/tmp')
zip_ref.close()
sys.path.append(os.path.join(os.path.dirname(__file__), "/tmp/vendored"))

import numpy as np
import pymysql

print("Process Tweet")
from process_tweet import process_tweet
from sentiment import get_tweet_sentiment
print("Done processing Tweet")

from sklearn.naive_bayes import MultinomialNB
from sklearn.externals import joblib



class Pricing(object):
  """docstring for Pricing"""
  def __init__(self):
    self.clf = joblib.load(os.path.join(os.path.dirname(__file__),'model.pkl'))

  def _from_trump(self, tweet):
    _input = process_tweet(tweet)
    result = self.clf.predict(np.asarray([_input]))
    print(result)
    return round(result[0]) == 1

  def price(self, tweet, previous_price):
    price = previous_price
    if(self._from_trump(tweet)):
      sentiment = get_tweet_sentiment(tweet)
      if sentiment == 'positive':
        price += 10
      elif sentiment == 'neutral':
        price += 40
      else:
        price += 100
    return price
print("Ending Printing")

tweet_store_db = 'diac--tweets'

client = Cloudant('andycampbell92', 'T6U1C9jFdYeq', account='andycampbell92', connect=True)
session = client.session()
db = client[tweet_store_db]

def datetime_handler(x):
  if isinstance(x, datetime.datetime):
    return x.isoformat()
  return x

def crawler(obj):
  if isinstance(obj,dict):
    for key in obj.keys():
      crawler(obj[key])
  if isinstance(obj,list):
    for entry in obj:
      crawler(entry)
  else:
    if obj == None or obj == '':
      obj = 'None'


# {
#   "access_token": "EAACOEyRSvhABAGIy87ZBW2t2X4uknFHaUdA4GarUn40PhFYbGEhVLWmMR9gxDWZBiDkfZArEmMzOxSxckDe3nWbo5Q0itfcP5jVOCr4TBf0VhZB8nHpyobX5HryXVYZBZAeVLEX0rbFBFTgr3E2jinqGT6fVLRkcpIMkBCrbyGAvj0opgWvix2",
#   "token_type": "bearer",
#   "expires_in": 5184000
# }

def handle(event, context):
  access_token = "EAACOEyRSvhABAGIy87ZBW2t2X4uknFHaUdA4GarUn40PhFYbGEhVLWmMR9gxDWZBiDkfZArEmMzOxSxckDe3nWbo5Q0itfcP5jVOCr4TBf0VhZB8nHpyobX5HryXVYZBZAeVLEX0rbFBFTgr3E2jinqGT6fVLRkcpIMkBCrbyGAvj0opgWvix2"
  print(dir(facebook))
  graph = facebook.GraphAPI(access_token=access_token)

  result = None
  pricing = Pricing()
  # Connect to the database
  mysql_connection = pymysql.connect(
    host='dirtinacan.caygupzryjw1.us-east-1.rds.amazonaws.com',
    port=3306,
    user='root',
    password='jD*#*40m24d6',
    db='dirtinacan',
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
  )
  try:
    with mysql_connection.cursor() as cursor:
      # Read a single record
      # sql = "SELECT `id`, `price`, `created_at` FROM `data_points` WHERE `email`=%s"

      tweet_queue_sql = "SELECT `id`, `dynamo_tweet_id`, `tweet_date` FROM `tweet_queue` WHERE processed=0 ORDER BY `id` DESC LIMIT 1"
      # cursor.execute(tweet_queue_sql, ('webmaster@python.org',))
      cursor.execute(tweet_queue_sql)
      tweet_queue_query = cursor.fetchall()
      tweet_queue_query = json.loads(json.dumps(tweet_queue_query, default=datetime_handler))
      print(tweet_queue_query)

      data_points_sql = "SELECT `id`, `price`, `dynamo_tweet_id` FROM `data_points` ORDER BY `id` DESC LIMIT 1"
      # cursor.execute(data_points_sql, ('webmaster@python.org',))
      cursor.execute(data_points_sql)
      data_points_query = cursor.fetchall()
      data_points_query = json.loads(json.dumps(data_points_query, default=datetime_handler))

      print(data_points_query)

      entry = {
        "price": None,
        "dynamo_tweet_id" : None
      }

      if len(tweet_queue_query): 
        entry["dynamo_tweet_id"] = tweet_queue_query[0]["dynamo_tweet_id"]
        update_sql = 'UPDATE tweet_queue SET processed = TRUE WHERE id={};'.format(tweet_queue_query[0]['id'])
        cursor.execute(update_sql)
        mysql_connection.commit()
        try:
          query = Query(db, selector={"id": int(tweet_queue_query[0]['dynamo_tweet_id'])})
          docs = query()['docs']
          twitter_link = 'https://twitter.com/realDonaldTrump/status/{}'.format(tweet_queue_query[0]["dynamo_tweet_id"])
          attachment =  {
            'name': 'Twitter Link',
            'link': twitter_link
          }

          price = pricing.price(docs[0]['full_text'], 0)
          message = "By the looks of things, Trump's PR team is posting for him again. No increase in Dirt in a Can price."
          if price == 10:
            message = "Our algorithm shows that Trump just posted something not that bad! ${} increase in Dirt in a Can price.".format(str(round(price/100.0, 2)))
          elif price == 40:
            message = "Our algorithm shows that Trump just posted something not everyone will like... ${} increase in Dirt in a Can price.".format(str(round(price/100.0, 2)))
          elif price == 100:
            message = "Our algorithm shows that Trump just posted something that will annoy a lot of people. ${} increase in Dirt in a Can price.".format(str(round(price/100.0, 2)))

          graph.put_wall_post(message=message, attachment=attachment, profile_id='1921359611412250')
          twitter.statuses.update(status=message + " " + twitter_link)
        except Exception, e:
          pass

        # pricing.price(docs[0]['full_text'])

      if len(data_points_query):
        if 'dynamo_tweet_id' in data_points_query[0] and data_points_query[0]['dynamo_tweet_id'] and not data_points_query[0]['dynamo_tweet_id'] == 'None':
          query = Query(db, selector={"id": int(data_points_query[0]['dynamo_tweet_id'])})
          docs = query()['docs']
          entry["price"] = pricing.price(docs[0]['full_text'], data_points_query[0]['price'])
        else:
          entry["price"] = max(data_points_query[0]['price'] - 10, 200)
      else:
        entry["price"] = 200

      insert_sql = "INSERT INTO data_points (`price`, `dynamo_tweet_id`) VALUES ('{0}', '{1}')".format(entry["price"], entry["dynamo_tweet_id"])
      print(insert_sql)
      cursor.execute(insert_sql)
      mysql_connection.commit()
      mysql_connection.close()
  except Exception as e:
    print(e)
    return {"message": "Error in request"}
  return {"message": "Request complete"}

if os.environ.get("ENV") == "development":
  handle({'id': '855055509455593472'}, None)
