# -*- coding: utf-8 -*-

# Add pip modules folder to path so we can see installed modules
import sys, os, datetime, json, random
sys.path.append(os.path.join(os.path.dirname(__file__), "pip_modules"))
import boto3, zipfile

from cloudant.client import Cloudant
from cloudant.query import Query

import facebook

print("Starting Printing")
# Add pip modules folder to path so we can see installed modules
s3 = boto3.resource('s3')
downloaded_zip = '/tmp/tensorflow.zip'
print("Downloading tensorflow")
s3.meta.client.download_file('python-modules-diac', 'tensorflow.zip', downloaded_zip)
print("Finished downloading tensorflow")
zip_ref = zipfile.ZipFile(downloaded_zip, 'r')
zip_ref.extractall('/tmp')
zip_ref.close()
sys.path.append(os.path.join(os.path.dirname(__file__), "/tmp/vendored"))

import numpy as np
import pymysql

print("Process Tweet")
from process_tweet import process_tweet
from sentiment import get_tweet_sentiment
print("Done processing Tweet")

from sklearn.naive_bayes import MultinomialNB
from sklearn.externals import joblib



class Pricing(object):
  """docstring for Pricing"""
  def __init__(self):
    self.clf = joblib.load(os.path.join(os.path.dirname(__file__),'model.pkl'))

  def _from_trump(self, tweet):
    _input = process_tweet(tweet)
    result = self.clf.predict(np.asarray([_input]))
    print(result)
    return round(result[0]) == 1

  def price(self, tweet, previous_price):
    price = previous_price
    if(self._from_trump(tweet)):
      sentiment = get_tweet_sentiment(tweet)
      if sentiment == 'positive':
        price += 10
      elif sentiment == 'neutral':
        price += 40
      else:
        price += 100
    return price
print("Ending Printing")

tweet_store_db = 'diac--tweets'

client = Cloudant('andycampbell92', 'T6U1C9jFdYeq', account='andycampbell92', connect=True)
session = client.session()
db = client[tweet_store_db]

def datetime_handler(x):
  if isinstance(x, datetime.datetime):
    return x.isoformat()
  return x

def crawler(obj):
  if isinstance(obj,dict):
    for key in obj.keys():
      crawler(obj[key])
  if isinstance(obj,list):
    for entry in obj:
      crawler(entry)
  else:
    if obj == None or obj == '':
      obj = 'None'


def handle(event, context):

  result = None
  pricing = Pricing()
  # Connect to the database
  mysql_connection = pymysql.connect(
    host='dirtinacan.caygupzryjw1.us-east-1.rds.amazonaws.com',
    port=3306,
    user='root',
    password='jD*#*40m24d6',
    db='dirtinacan',
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
  )
  try:
    with mysql_connection.cursor() as cursor:
      # Read a single record
      # sql = "SELECT `id`, `price`, `created_at` FROM `data_points` WHERE `email`=%s"
      data_points_sql = "SELECT * FROM `data_points` ORDER BY `id` ASC"
      cursor.execute(data_points_sql)
      data_points_query = cursor.fetchall()
      data_points_query = json.loads(json.dumps(data_points_query, default=datetime_handler))
      # data_points_query = data_points_query[358:] # This where things start making sense

      # print(len(data_points_query))
      # new_price = data_points_query[0]['price']
      # for index, data_point in enumerate(data_points_query):
      #   if(index + 1 > len(data_points_query)):
      #     break

      #   next_id = data_points_query[index+1]['id']
      #   if 'dynamo_tweet_id' in data_point and data_point['dynamo_tweet_id'] and not data_point['dynamo_tweet_id'] == 'None':
      #     query = Query(db, selector={"id": int(data_point['dynamo_tweet_id'])})
      #     docs = query()['docs']
      #     new_price = pricing.price(docs[0]['full_text'], new_price)
      #   else:
      #     new_price = max(new_price - 10, 200)

      #   print(new_price)

      #   update_sql = 'UPDATE data_points SET price={} WHERE id={};'.format(new_price,next_id)
      #   cursor.execute(update_sql)
      #   mysql_connection.commit()
      data_points_query = data_points_query[:358] # This where things start making sense

      print(len(data_points_query))
      new_price = 200 + random.randint(0,100)
      for index, data_point in enumerate(data_points_query):
        if(index + 1 > len(data_points_query)):
          break

        next_id = data_points_query[index+1]['id']
        new_price = 200 + random.randint(0,100)
        update_sql = 'UPDATE data_points SET price={} WHERE id={};'.format(new_price,next_id)
        cursor.execute(update_sql)
        mysql_connection.commit()
      mysql_connection.close()

  except Exception as e:
    print({"message": "Error in request"})
    print(e)
    return {"message": "Error in request"}
  return {"message": "Request complete"}

if os.environ.get("ENV") == "development":
  handle({'id': '855055509455593472'}, None)
