# -*- coding: utf-8 -*-

# Add pip modules folder to path so we can see installed modules
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), "pip_modules"))
import time, boto3, pymysql, pymysql.cursors, json
from cloudant.client import Cloudant
from cloudant.query import Query

tweet_queue_table = 'tweet_queue'
tweet_store_db = 'diac--tweets'
trump_user_id = '25073877'

client = Cloudant('andycampbell92', 'T6U1C9jFdYeq', account='andycampbell92', connect=True)
session = client.session()
db = client[tweet_store_db]

from twitter import *
twitter = Twitter(
  auth=OAuth(
    "448986414-Iy9HzUxVCF4I0YIkIDBAUOZutdadZp3uJ2Cu7MJF",
    "rpY09yf1cZKUD0wh6c6eM8TkvaT3oefgjQE0hIyqPjxAN",
    "U65bTXrEEtoi4cXE3BbO5EohU",
    "PnBxisnT2fnnDrmu6HGFJflMtPzlAZLLFKpxGySS2350pf3Mfw"
  )
)

mysql_connection = pymysql.connect(
  host='dirtinacan.caygupzryjw1.us-east-1.rds.amazonaws.com',
  port=3306,
  user='root',
  password='jD*#*40m24d6',
  db='dirtinacan',
  charset='utf8mb4',
  cursorclass=pymysql.cursors.DictCursor
)

def unseen_tweets(tweet_ids):
  prepped_tweet_ids = "','".join(str(tid) for tid in tweet_ids)
  stored_tweet_sql = "SELECT `dynamo_tweet_id` FROM `{0}` WHERE `dynamo_tweet_id` IN ('{1}')".format(tweet_queue_table,prepped_tweet_ids)
  cursor = mysql_connection.cursor(pymysql.cursors.DictCursor)
  cursor.execute(stored_tweet_sql)
  stored_tweet_ids = [stored_tweet['dynamo_tweet_id'] for stored_tweet in cursor]

  return (set(tweet_ids) - set(stored_tweet_ids))

def store_tweet(tweet):
  db.create_document(tweet)
  created_at = tweet_timestamp(tweet['created_at'])
  cursor = mysql_connection.cursor(pymysql.cursors.DictCursor)
  insert_sql = "INSERT INTO `{0}` (`dynamo_tweet_id`, `tweet_date`, `processed`) VALUES ('{1}', '{2}', FALSE)".format(tweet_queue_table, str(tweet['id']), time.strftime('%Y-%m-%d %H:%M:%S', created_at))
  cursor.execute(insert_sql)
  mysql_connection.commit()

def tweet_timestamp(tweet_created_at):
  return time.strptime(tweet_created_at,'%a %b %d %H:%M:%S +0000 %Y')

def handle(event, context):
  timeline = twitter.statuses.user_timeline(user_id=trump_user_id, count = 200)
  tweet_ids = [str(tweet['id']) for tweet in timeline]
  unseen_tweet_ids = unseen_tweets(tweet_ids)
  for tweet in timeline:
    if str(tweet['id']) in unseen_tweet_ids:
      full_tweet = twitter.statuses.show(_id=tweet['id'], tweet_mode='extended')
      store_tweet(full_tweet)

  print "{0} new tweets".format(len(unseen_tweet_ids))
  return "{0} new tweets".format(len(unseen_tweet_ids))

if os.environ.get("ENV") == "development":
  handle(None, None)
