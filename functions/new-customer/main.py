# -*- coding: utf-8 -*-

# Add pip modules folder to path so we can see installed modules
import sys, os, json
from random import shuffle

sys.path.append(os.path.join(os.path.dirname(__file__), "pip_modules"))
import stripe

stripe.api_key = "sk_test_3ckK6VP7I08hjgym5B2uOxkF"

def handle(event, context):

  # result = json.dumps({"event": event})
  # Connect to the database
  shipping = {
    "name": event["name"],
    "address": event["address"]
  }
  customer = stripe.Customer.create(
    description="Customer for {}".format(event["email"]),
    source=event["stripeToken"], # obtained with Stripe.js
    email=event["email"],
    shipping=shipping
  )

  # Charge the Customer instead of the card:
  charge = stripe.Charge.create(
    amount=event["amount"],
    currency="usd",
    description="Payment for a dirt in a can",
    customer=customer.id
  )

  return {"message": 'Yay'}

if os.environ.get("ENV") == "development":
  handle({'amount': 10,'stripeToken':'sflasjfddaslfj4l3433434'}, None)
