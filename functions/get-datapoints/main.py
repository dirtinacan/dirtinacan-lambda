# -*- coding: utf-8 -*-

# Add pip modules folder to path so we can see installed modules
import sys, os, datetime, json
sys.path.append(os.path.join(os.path.dirname(__file__), "pip_modules"))
import pymysql, pymysql.cursors
from cloudant.client import Cloudant
from cloudant.query import Query

tweet_store_db = 'diac--tweets'

client = Cloudant('andycampbell92', 'T6U1C9jFdYeq', account='andycampbell92', connect=True)
session = client.session()
db = client[tweet_store_db]

def datetime_handler(x):
  if isinstance(x, datetime.datetime):
    return x.isoformat()
  return x

def handle(event, context):
  result = None
  # Connect to the database
  mysql_connection = pymysql.connect(
    host='dirtinacan.caygupzryjw1.us-east-1.rds.amazonaws.com',
    port=3306,
    user='root',
    password='jD*#*40m24d6',
    db='dirtinacan',
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
  )
  try:
    with mysql_connection.cursor() as cursor:
      sql = "SELECT `id`, `price`, `dynamo_tweet_id` ,`created_at` FROM `data_points` ORDER BY `created_at`"
      cursor.execute(sql)
      result = cursor.fetchall()
      result = json.loads(json.dumps(result, default=datetime_handler))

      # for doc in result:
      #   try:
      #     query = Query(db, selector={"id": {"$in": [int(doc["dynamo_tweet_id"])]}})
      #     docs = query()['docs']
      #     doc['tweet'] = None if not len(docs) else docs[0]
      #   except Exception as e:
      #     print(e)

      array_query = [int(x["dynamo_tweet_id"]) for x in list(filter(lambda x: x["dynamo_tweet_id"] and not x["dynamo_tweet_id"] == 'None', result))]
      query = Query(db, selector={"id": {"$in": array_query}})
      docs = query()['docs']

      print(docs)

      hash_map = {}
      for doc in docs:
        hash_map[str(doc['id'])] = doc

      doc['tweet'] = None if not len(docs) else docs[0]
      for entry in result:
        entry['tweet'] = None if not entry['dynamo_tweet_id'] or entry['dynamo_tweet_id'] == "None" else hash_map[entry['dynamo_tweet_id']]

      mysql_connection.close()
  except Exception as e:
    print("Error: ", e)


  return {"result": result}

if os.environ.get("ENV") == "development":
  print(handle({}, None))
