FROM lambci/lambda:build
RUN curl https://raw.githubusercontent.com/apex/apex/master/install.sh | sh
RUN curl https://bootstrap.pypa.io/get-pip.py | python
COPY .aws /root/.aws
ENV ENV=development
WORKDIR /usr/src/app